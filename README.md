# Template for webpage

This readme outlines the basic template for a webpage on my personal website.

This code goes at the begining of every page, however change the title as necessery.
```
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="/style.css" rel="stylesheet" type="text/css" media="all">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Welcome to Chris Were's website</title>
</head>
<body>
  <header><a href="/">Chris Were's website</a></header>
  <!--HEADER ENDS, BODY BEGINS-->
```

This code goes at the end of every page, they're the closing tags.
```
  <!--BODY ENDS, FOOTER BEGINS-->
</body>
</html>
```